# Only used to regenerate the man page. Installation is done separately.

MANFILES=cfdoc.1

#-------------------------------------------------------------------------------
MD_TO_MAN = ronn --roff --organization MA --manual AWStools --date `date +'%Y-%m-%d'`

%.1:	%.md
	$(MD_TO_MAN) $<

#-------------------------------------------------------------------------------
man:	$(MANFILES)

install:
	./install.sh

clean:	
	$(RM) $(MANFILES)
