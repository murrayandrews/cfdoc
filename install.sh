#!/bin/bash
# Install cfdoc.

TARGET=cfdoc
MANFILES=cfdoc.1
python=python3  # Prefer python or python3.
force_python_version=  # Set to not-empty value to prevent choice of version

# ##############################################################################
# region BOILERPLATE

PROG=`basename $0`
BASE=/usr/local

echo="echo -e"
install="install -Sv"

ANSI_GREY='\033[2m'
ANSI_RESET='\033[0m'
ANSI_BLUE='\033[34m'
ANSI_RED='\033[31m'
ANSI_YELLOW='\033[33m'
ANSI_MAGENTA='\033[35m'

# ------------------------------------------------------------------------------
# Print an error message
function error() {
	$echo "${ANSI_RESET}${ANSI_RED}$*${ANSI_RESET}"
}

# ------------------------------------------------------------------------------
# Confirm a value with the user
# Usage: checkval prompt default
function checkval() {
	$echo "${ANSI_BLUE}$1 ${ANSI_RESET}${ANSI_GREY}($2)${ANSI_RESET}" \\c >/dev/tty
	read val
	if [ "$val" == "" ]
	then
		$echo $2
	else
		$echo "$val"
	fi
}

# ------------------------------------------------------------------------------
# Print a message and run a command. Exits on fail.
# Usage: saydo message command [args...]
function saydo() {
	$echo "${ANSI_BLUE}$1${ANSI_RESET}" ... \\c 
	shift
	result=`$* 2>&1`
	if [ $? -eq 0 ]
	then
		$echo ${ANSI_BLUE}Done${ANSI_RESET}
		[ "$result" != "" ] && $echo "${ANSI_GREY}$result${ANSI_RESET}"
	else
		error Failed
		[ "$result" != "" ] && $echo "${ANSI_YELLOW}$result${ANSI_RESET}"
		error Abort && exit 2
	fi
}

# ------------------------------------------------------------------------------
# Convert relative to absolute path. Usage: abspath path
function abspath() {
	$python -c "from __future__ import print_function; import os.path;print(os.path.abspath('$1'))"
}

# ------------------------------------------------------------------------------
# Check that required programs are avalable
# Usage: require prog ...
function require() {
	missing=0
	for p
	do
		[[ `which $p` == "" ]] && error "$p is required but cannot be found" && missing=1
	done
	[ $missing -ne 0 ] && exit 2
}
	
# ------------------------------------------------------------------------------
# Arguments check.
[ $# -gt 1 ] && $echo Usage: $PROG [basedir] && exit 1
case "$1"
in
	-*)	$echo Usage: $PROG [basedir]; exit 0;;
	"")	;;
	*)	BASE="$1";;
esac

# ------------------------------------------------------------------------------
# Warn if not root. May or may not be a problem.
if  [ `id -u` -ne 0 ]
then
	$echo ${ANSI_MAGENTA}WARNING: You are not root. This may not work. Interrupt or, to continue, hit return.${ANSI_RESET} \\c
	read x
else
	umask 022
fi

# ------------------------------------------------------------------------------
# Confirm python version
[ "$force_python_version" == "" ] && python=`checkval "python3 or python" $python`

# ------------------------------------------------------------------------------
# Check program prerequisites
require $python virtualenv

# ------------------------------------------------------------------------------
# Validate and create primary directories
BINDIR=`checkval BINDIR $BASE/bin`
LIBDIR=`checkval LIBDIR $BASE/lib`
MANDIR=`checkval MANDIR $BASE/share/man/man1`
BINTARGET=$BINDIR/$TARGET
LIBTARGET=$LIBDIR/$TARGET

# Prevent name clash between main script and library directory
[ `abspath $BINTARGET` == `abspath $LIBTARGET` ] && LIBTARGET=${LIBTARGET}.d

saydo "Creating primary directories" mkdir -p  $BINDIR $LIBTARGET $MANDIR

# ------------------------------------------------------------------------------
# Prepare the main runner - generic shell script that links to the python TARGET
TMPFILE=/tmp/$PROG.$$
ABSLIBDIR=`abspath $LIBDIR`
trap "/bin/rm -f $TMPFILE; exit 3" 0
sed "s|^LIBDIR=.*|LIBDIR=${ABSLIBDIR}|" runner.sh > $TMPFILE

# Install the main runner and the main Python script
saydo "Installing main runner" $install -m755 $TMPFILE $BINTARGET
/bin/rm -f $TMPFILE
trap '' 0
saydo "Installing ${TARGET}.py" $install -m755 ${TARGET}.py $LIBTARGET

# Install the man files
[ "$MANFILES" != "" ] && saydo "Installing man page(s)" $install -m644 $MANFILES $MANDIR

# ------------------------------------------------------------------------------
# Presence of a requirements.txt file will force creation of a virtualenv.
if [ -f requirements.txt ]
then
	saydo "Installing requirements.txt" $install -m644 requirements.txt $LIBTARGET
	saydo "Creating virtualenv in $LIBTARGET" virtualenv --python=$python $LIBTARGET/venv
	. $LIBTARGET/venv/bin/activate
	saydo "Upgrading pip" pip install pip --upgrade
	saydo "Configuring virtualenv" pip install --upgrade -r requirements.txt
fi

# endregion BOILERPLATE
# ##############################################################################

# ##############################################################################
# region Custom components

saydo "Creating plugin and template directories" mkdir -p $LIBTARGET/plugins $LIBTARGET/templates
saydo "Installing plugins" $install -m644 plugins/*.py $LIBTARGET/plugins
saydo "Installing templates" $install -m644 templates/* $LIBTARGET/templates
saydo "Installing support files" $install -m644 licence.txt README.md $LIBTARGET
