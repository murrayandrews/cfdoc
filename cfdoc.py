#!/usr/bin/env python3

"""
Extract documentation from a CloudFormation template.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function

import argparse
import importlib
import json
import logging
import logging.handlers
import operator
import os
import yaml
import re
import sys
from collections import OrderedDict
from datetime import datetime
from os.path import basename, dirname, splitext

import jinja2

__author__ = 'Murray Andrews'

PROG = splitext(basename(sys.argv[0]))[0]
CFDOCPATH = ':templates:/usr/local/lib/cfdoc/templates'
PLUGINDIR = ''
CFD_PLUGIN = 'cfd_plugin'  # Callable in plugins
CFD_URL = 'https://bitbucket.org/murrayandrews/cfdoc'

# ------------------------------------------------------------------------------
# Minimal Python 2/3 support

PY3 = sys.version_info[0] == 3

if PY3:
    string_types = str
else:
    # noinspection PyCompatibility, PyUnresolvedReferences
    string_types = basestring

# ------------------------------------------------------------------------------
# Clunky support for colour output if colorama is not installed.

try:
    # noinspection PyUnresolvedReferences
    import colorama
    # noinspection PyUnresolvedReferences
    from colorama import Fore, Style

    colorama.init()

except ImportError:
    class Fore(object):
        """
        Basic alternative to colorama colours using ANSI sequences
        """
        RESET = '\033[0m'
        BLACK = '\033[30m'
        RED = '\033[31m'
        GREEN = '\033[32m'
        YELLOW = '\033[33m'
        BLUE = '\033[34m'
        MAGENTA = '\033[35m'
        CYAN = '\033[36m'


    class Style(object):
        """
        Basic alternative to colorama styles using ANSI sequences
        """
        RESET_ALL = '\033[0m'
        BRIGHT = '\033[1m'
        DIM = '\033[2m'
        NORMAL = '\033[22m'

# Keys containing documentation elements. A couple of variants are accepted but
# the first one is preferred. Must be in CloudFormation template Metadata.

K_CFDOC = ('CFdoc', 'Cfdoc', 'cfdoc')

LOG = logging.getLogger()


# -------------------------------------------------------------------------------
def import_by_name(name, parent=None):
    """
    Import a named module from within the named parent.

    :param name:            The name of the sender module.
    :param parent:          Name of parent module. Default None.

    :type name:             str
    :type parent:           str

    :return:                The sender module.
    :rtype:                 module

    :raise ImportError:     If the import fails.
    """

    if parent:
        name = parent + '.' + name

    return importlib.import_module(name)


# -------------------------------------------------------------------------------
def mget(d, keys, default=None):
    """
    Lookup each of the keys in the dict d and return the first non-None value or
    the default if none of the keys have a non-None value.

    :param d:           A dict.
    :param keys:        An iterable containing possible keys for dict d.
    :param default:     Value to return if none of the keys are present.
                        Default None.
    :type d:            dict
    :type keys:         list | tuple
    :type default:      T

    :return:            The value for the first avaiable key of the default.
    :rtype:             T
    """

    for k in keys:
        v = d.get(k)
        if v is not None:
            return v

    return default


# ------------------------------------------------------------------------------
class CFdoc(object):
    """
    Class to represent the documentation elements extracted from a CloudFormation
    template. This structure is loaded from a CloudFormation template file, then
    passed to any available and enabled plugins for manipulation, then sent to
    a (Jinja2) rendering engine.

    The following instance attributes are potentially useful to plugins and
    Jinja2 templates:

    template:   The data structure holding the full JSON of the CloudFormation
                base template.

    name:       Name of the template, typically derived from the name of the
                CloudFormation template file.

    title:      Derived from the main Description tag in the CF template.

    overview:   An ordered dictionary of data items extracted from the CFdoc
                tag in the main Metadata tag for the CF template. There are a
                bunch of "standard" items that are located first, then any
                other tag the template author cares to add will also be
                included. With the exception of the special tag 'Groups', the
                values of these keys must be either a string or a list of
                strings.

                "Ordered" here means:

                    - Standard keys in a fixed order.

                    - User defined keys in alphabetic order.

                This is done so that the generated documentation always presents
                template information in a fixed arrangement.

                The optional Groups tag is used to assign resources to resource
                groups for documentation purposes. Its value must be a list of
                dictionaries. Each dictionary must contain an 'Id' and a 'Name'
                key. The former is the group ID and can be referenced by 'Group'
                keys in the CFdoc entries for resources.  The 'Name' key should
                be a short display name for the group.  Typically, documentation
                rendering templates will render present resource groups in the
                order in which they are defined in the 'Groups' list.

    parameters: An ordered dictionary of parameters from the CloudFormation
                template. Keys are parameter names and the values are
                ordered dictionaries containing 'Type' and 'Description' keys.
                The latter is derived from the parameter specification in the
                CF template by combining the parameter 'Description' and
                'ConstraintDescription' elements.

    resources:  An ordered dictionary containing the documentation items for each
                resource in the CloudFormation template. Keys are the resource
                names as given in the CF template. Values are ordered dictionaries
                of information items about the resource. Ordered in this case
                means:

                    - Type key first.

                    - Standard keys in a fixed order

                    - User defined keys in alphabetic order.

                This is done so that the generated documentation always presents
                resource information in a fixed arrangement for each resource.

                The 'Type' item is extracted from the 'Type' key in the CF
                resoource specification. All other items are extracted from the
                CFdoc tag in the Metadata for the resource. With the exception
                of the Group key, the values for the items in the CFdoc tag
                must be a string or a list of strings. The value of the Group
                key must be a string and should consist of one of the group IDs
                from the 'Groups' tag in the main CF template Metadata/CFdoc.

    resource_groups:
                An ordered dictionary of resource groups defined in the 'Groups'
                key in the main template Metadata/CFdoc. The order in which
                the groups were listed in the 'Groups' key is preserved. The
                value of each item is a dictionary containing a 'Name' key
                (the group Name) and a 'Members' key. The latter is a list of
                resource names in the group, sorted by resource name. This is
                also a key into the resources attribute described above.

                In addition to the explicitly specified groups, there may also
                be a group with a key of None, holding resources not otherwise
                assigned to a defined group. Plugins seeking to manipulate
                the group information must take care to maintain referential
                integrity.

                Note that some groups may have no members.

    outputs:    An (alphabetically) ordered dictionary of documentation for the
                'Outputs' defined in the CloudFormation template. Keys are
                output names and values are ordered dictionaries, currently,
                containing only a 'Description' key with a value derived from
                the CF template.

    plugins:    The structures described above (apart from the template attribute)
                can be manipulated by plugins (including adding additional fields
                to the information for individual resources and parameters
                defined above).

                Plugins can also provide additional information in the plugins
                dictionary attribute. Well behaved plugins will create an entry
                in the plugins dictionary with the plugin name as the key. What
                happens under that is plugin specific.

    """

    # Standard template overview tags.
    OVERVIEW_TAGS = [
        'Version', 'Description', 'Author', 'Licence', 'License',
        'Prerequisities', 'Changes',
    ]

    OVERVIEW_RESERVED_TAGS = ['Groups']

    # Standard template resource tags. Printed in the order given.
    RESOURCE_TAGS = [
        'Description', 'Dependencies', 'Warnings',
    ]

    RESOURCE_RESERVED_TAGS = ['Group']

    # --------------------------------------------------------------------------
    def __init__(self, name, template):
        """
        :param name:        The template name (typically a file name). Used only
                            for labelling so content is arbitrary.
        :param template:    A template object. Must be a dict containing the
                            elements expected in a CloudFormation template.
        :type template:     dict
        :raise ValueError:  If the object is not a dict.
        """

        if not isinstance(template, dict):
            raise ValueError('Malformed template: expected dict, got {}'.format(type(template)))

        self._template = template
        self.name = name
        self.title = None
        self.resources = OrderedDict()  # The documentation components of each resource
        self.resource_groups = OrderedDict()  # Resource resource_groups
        self.overview = OrderedDict()  # Template level doc items
        self.parameters = OrderedDict()
        self.outputs = OrderedDict()
        self.plugins = {}  # Allow plugins to add their own data elements.

        self.resource_groups[None] = {
            'Name': 'Ungrouped Resources',
            'Members': []
        }

        # ----------------------------------------
        # Make sure it is a CloudFormation template

        if not template.get('AWSTemplateFormatVersion'):
            raise ValueError('{}: No AWSTemplateFormatVersion key'.format(name))

        # ----------------------------------------
        # Process the template
        self._process_header()
        self._process_params()
        self._process_resources()
        self._process_outputs()

    # --------------------------------------------------------------------------
    def _process_header(self):
        """
        Process the header section of the template. This consists of the main
        "Description" key and the main "
        """

        LOG.debug('Processing CloudFormation template header')

        # ----------------------------------------
        # One line title is taken from the template "Description" tag.
        try:
            self.title = self._template['Description']
        except KeyError:
            LOG.warning('No template Description key')
            self.title = 'Template {}'.format(self.name)

        # ----------------------------------------
        # Look for a Metadata section containing a CFdoc key
        metadata = self._template.get('Metadata')
        if not metadata:
            LOG.warning('No template Metadata key')
            return

        cfdoc = mget(metadata, K_CFDOC)
        if not cfdoc:
            LOG.warning('No {} key in template metadata'.format(K_CFDOC[0]))
            return

        # ----------------------------------------
        # Retrieve the standard CFdoc tags. We do this first to preserve ordering
        for key in self.__class__.OVERVIEW_TAGS:
            if cfdoc.get(key):
                self.overview[key] = paragraphs(cfdoc.get(key))

        # Pickup any additional fields in alphabetical order (ignore reserved metatags)
        for key in sorted(set(cfdoc) - set(self.overview) - set(self.__class__.OVERVIEW_RESERVED_TAGS)):
            self.overview[key] = paragraphs(cfdoc.get(key))

        # ----------------------------------------
        # Look for an optional resource groups key which is a list of resource
        # groups. Each element is an object containing "Id" and "Name" keys.
        # Individual resources can point to the Id.
        # There is a default None group for ungrouped resources.

        rsrc_group_list = cfdoc.get('Groups', [])
        if not isinstance(rsrc_group_list, list):
            LOG.error('Malformed resource group list in main {} key'.format(K_CFDOC[0]))
        else:
            for group in rsrc_group_list:
                if not isinstance(group, dict):
                    LOG.error('Malformed resource group: {}'.format(group))
                    continue
                rgrp_id = group.get('Id')
                if not rgrp_id:
                    print(80 * '[')
                    LOG.error('Resource group with no Id: {}'.format(group))
                    continue
                self.resource_groups[rgrp_id] = {
                    'Name': group.get('Name', rgrp_id),
                    'Members': []
                }

    # --------------------------------------------------------------------------
    def _process_params(self):
        """
        Process any CF template parameters.

        """

        LOG.debug('Processing CloudFormation template parameters')

        params = self._template.get('Parameters')
        if not params:
            return

        for p in sorted(params):
            v = params[p]
            self.parameters[p] = OrderedDict()
            self.parameters[p]['Type'] = v.get('Type', '?')
            self.parameters[p]['Description'] = \
                paragraphs(v.get('Description')) + paragraphs(v.get('ConstraintDescription'))

    # --------------------------------------------------------------------------
    def _process_resources(self):
        """
        Process resource definitions in the CF template. These may have Group
        attribute to assign them to a resource group.
        """

        LOG.debug('Processing CloudFormation template resources')

        cf_resources = self._template.get('Resources')
        if not cf_resources:
            LOG.error('No Resources key')
            return

        for rsrc_id in sorted(cf_resources):
            if rsrc_id in self.resources:
                LOG.warning('Resource {}: duplicate ignored'.format(rsrc_id))
                continue

            # ----------------------------------------
            # Extract the cfdoc for the resource from the metadata tag

            cfdoc = {}
            rsrc = cf_resources[rsrc_id]
            if not isinstance(rsrc, dict):
                LOG.error('Resource {}: object expected - skipping'.format(rsrc_id))
                continue

            metadata = rsrc.get('Metadata')
            if not metadata:
                LOG.info('Resource {}: no Metadata key'.format(rsrc_id))
            else:
                cfdoc = mget(metadata, K_CFDOC, {})
                if not cfdoc:
                    LOG.info('Resource {}: no {} key in Metadata'.format(rsrc_id, K_CFDOC[0]))

            # ----------------------------------------
            # Create an empty dict for the resource documentation attributes.
            rsrc_info = OrderedDict()

            # ----------------------------------------
            # Extract the resource type
            rsrc_info['Type'] = rsrc.get('Type')
            if not rsrc_info['Type']:
                LOG.error('Resource {}: no resource type specified'.format(rsrc_id))

            # ----------------------------------------
            # Get the resource group id for this resource. If its not a known group
            # we create a new resource group but issue a warning. The resource group may be None.
            rsrc_info['Group'] = rsrc_group = cfdoc.get('Group')
            if rsrc_group not in self.resource_groups:
                LOG.warning('Resource {}: unknown resource group {}'.format(
                    rsrc_id, rsrc_group))
                self.resource_groups[rsrc_group] = {
                    'Name': rsrc_group,
                    'Members': []
                }

            self.resource_groups[rsrc_group]['Members'].append(rsrc_id)

            # ----------------------------------------
            # Look for the cfdoc standard tags
            for key in self.__class__.RESOURCE_TAGS:
                rsrc_info[key] = paragraphs(cfdoc.get(key))

            # Pickup any additional fields in alphabetical order (ignore reserved tags)
            for key in sorted(set(cfdoc) - set(rsrc_info) - set(self.__class__.RESOURCE_RESERVED_TAGS)):
                rsrc_info[key] = paragraphs(cfdoc.get(key))

            self.resources[rsrc_id] = rsrc_info

            # ----------------------------------------
            # Delete any empty resource groups
            # for rsrc_group in self.resource_groups:
            #     if not self.resource_groups[rsrc_group]['Members']:
            #         del self.resource_groups[rsrc_group]

    # --------------------------------------------------------------------------
    def _process_outputs(self):
        """
        Process output definitions in the CloudFormation template.

        """

        LOG.debug('Processing CloudFormation template outputs')

        outputs = self._template.get('Outputs')
        if not outputs:
            return

        for op in sorted(outputs):
            v = outputs[op]
            self.outputs[op] = OrderedDict()
            self.outputs[op]['Description'] = v.get('Description')
            self.outputs[op]['Export'] = v.get('Export', {}).get('Name')
            if not self.outputs[op]['Description']:
                LOG.warning('Output {}: no Description key'.format(op))

    # --------------------------------------------------------------------------
    @property
    def template(self):
        """
        Return the cloudformation template object.

        :return:    The template object.
        :rtype:     dict[str, T]
        """

        return self._template

    # --------------------------------------------------------------------------
    def render(self, fmt):
        """
        Render the CFdoc object using the specified format. The format is used
        to determine the name of a Jinja2 template file.

        :param fmt:     Format name. A file 'fmt.jinja2' must exist in
                        the CFDOC path (specified by the environment variable
                        CFDOCPATH.
        :type fmt:      str
        """

        jtemplate = fmt + '.jinja2'
        jinja_path = os.environ.get('CFDOCPATH', CFDOCPATH).split(':')
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(jinja_path))
        env.filters['re_match'] = j2_re_match
        env.filters['asort'] = j2_sort_multi_attributes
        env.filters['xref'] = j2_md_xref
        env.filters['e_md'] = j2_escape_markdown

        jtemplate = env.get_template(jtemplate)

        # Common parameters.
        common = {
            "prog": PROG,
            "project_url": CFD_URL,
            "now_iso": datetime.now().isoformat(),
            "now_ctime": datetime.now().ctime(),
            "utcnow_iso": datetime.utcnow().isoformat()
        }

        return jtemplate.render(cfdoc=self, common=common)


# ------------------------------------------------------------------------------
def get_log_level(s):
    """
    Convert the string version of a log level defined in the logging module to the
    corresponding log level. Raises ValueError if a bad string is provided.

    :param s:       A string version of a log level (e.g. 'error', 'info').
                    Case is not significant.
    :type s:        str

    :return:        The numeric logLevel equivalent.
    :rtype:         int

    :raises:        ValueError if the supplied string cannot be converted.
    """

    if not s or not isinstance(s, str):
        raise ValueError('Bad log level:' + str(s))

    t = s.upper()

    if not hasattr(logging, t):
        raise ValueError('Bad log level: ' + s)

    return getattr(logging, t)


# ------------------------------------------------------------------------------
def paragraphs(s):
    """
    Converts a string or list of strings into a list of paragraphs. An empty
    string denotes start of a new paragraph.

    :param s:       A string or list of strings. If None, treat it as an empty
                    paragraph.
    :type s:        str | list[str]
    :return:        A list of paragraph strings.
    :rtype:         list[str]
    """

    if not s:
        return []

    # noinspection PyUnresolvedReferences
    if isinstance(s, str):
        s = [s]
    elif not PY3 and isinstance(s, unicode):
        # noinspection PyUnresolvedReferences
        s = [s.decode('utf-8')]
    elif not isinstance(s, list):
        raise ValueError('Expected list of strings, got {}'.format(type(s)))

    para_list = []
    para = []

    for line in s:
        if not isinstance(line, string_types):
            raise TypeError('Expected a string, got {}: {}'.format(type(line), line))
        line = line.strip()
        if line:
            para.append(line)
        elif para:
            # Start a new paragraph
            para_list.append(' '.join(para))
            para = []

    if para:
        para_list.append(' '.join(para))

    return para_list


# ................................................................................
# region jinja filters

# ------------------------------------------------------------------------------
def j2_sort_multi_attributes(l, *operators):
    """
    Sort by multiple attributes.

    See: http://stackoverflow.com/questions/16143053/stable-sorting-in-jinja2

    Add to Jinja env:

        env = config.get_jinja2_environment()
        env.filters['asort'] = sort_multi_attributes

    Use in template:

        {{item_list|sort_multi('attr1','attr2')}}


    :param l:           The list to sort.
    :param operators:
    :type l:            list
    :return:            A sorted copy of the list.
    """

    l2 = list(l)
    l2.sort(key=operator.attrgetter(*operators))
    return l2


# ------------------------------------------------------------------------------
def j2_md_xref(s, style, target=None):
    """
    Convert a header text to a markdwon cross reference. BitBucket does this a
    different way to the markdown conventions. As usual. :-(

    Add to Jinja env:

        env = config.get_jinja2_environment()
        env.filters['xref'] = j2_md_xref

    Use in template:

        {{ header_text | xref('bitbucket', target='Some header') }}

    :param s:           The link text. Also used as the target header if
                        target is None.
    :param style:       Markdown style. Case insensitive. Either BitBucket (or
                        bb), Pandoc (pd) or anything else.
    :param target:      If not None, the target header
    :type s:            str
    :return:            A link construct [Text](#xref)
    :rtype:             str
    """

    if not target:
        target = s

    style = style.lower()
    if style in ('bitbucket', 'bb'):
        xref = 'markdown-header-' + '-'.join(target.lower().split())
    elif style in ('pandoc', 'pd'):
        xref = '-'.join(re.sub(r'[^\w\d\s-]+', '', target).lower().split())
    else:
        xref = ''.join(re.sub(r'[^\w\d\s:-]+', '', target).lower().split())
    return '[{}](#{})'.format(s, xref)


# ------------------------------------------------------------------------------
def j2_re_match(s, pattern):
    """
    This is a Jinja2 filter to search a string and return the first capture
    group. If no match returns None. If no capture group and there is a match,
    then the matched component is returned.

    WARNING: The parameters are the opposite way around from re.match() because
    of the way the Jinja2 extension API works.

    Add to Jinja env:

        env = config.get_jinja2_environment()
        env.filters['re_match'] = j2_re_match

    Use in template:

        {{ xxx | re_match(pattern) }}

    Note that re.search() is used so the pattern is not automatically anchored
    to the start of the string.

    :param s:       String to search.
    :param pattern: Regex.
    :type s:        str
    :type pattern:  str

    :return:        The matched component or None.
    :rtype:         str
    """

    m = re.match(pattern, s)
    if not m:
        return None

    return m.group(1 if m.groups() else 0)


# ------------------------------------------------------------------------------
MD_ESCAPE_CHARS_RE = re.compile(r'([\[\]`])')  # Replace [, ] and `


def j2_escape_markdown(s):
    """
    Jinja2 filter to replace Markdown special chars with escaped versions.

    Add to Jinja env:

        env = config.get_jinja2_environment()
        env.filters['e_md'] = j2_escape_markdown

    Use in template:

        {{ xxx | e_md }}


    :param s:       String to escape
    :type s:        str
    :return:        Escaped string
    :rtype:         str
    """

    return MD_ESCAPE_CHARS_RE.sub(r'\\\1', s)


# endregion jinja filters
# ................................................................................


# ------------------------------------------------------------------------------
def dofile(ofmt, ifp=sys.stdin, ofp=sys.stdout, name=None, plugins=None,
           params=None, ifmt='JSON'):
    """
    Process a CloudFormation template file.

    :param ofmt:    Required output format. There must be a file fmt.jinja2 in
                    the CFDOCPATH somewhere.
    :param ifp:     Input template file stream. Default stdin.
    :param ofp:     File stream for rendered output. Default stdout.
    :param name:    Template name. If None, the filename associated with the input
                    stream is used. Default None.
    :param plugins: A dict of plugins that post-process the CloudFormation doc
                    structure after loading and prior to rendering. Each value
                    must be a callable. If run order matters, use an OrderedDict.
    :param params:  A dictionary of parameters for the plugins. The keys
                    are the plugin names. Values are a dictionary of parameters
                    for the plugin that will be passed as kwargs to the entry
                    point.
    :param ifmt:    Format of CloudFormation file -- either 'JSON' or 'YAML'.
                    Default 'JSON'.

    :type ofmt:     str
    :type ifp:      file
    :type ofp:      file
    :type plugins:  dict
    :type params:   dict[s, dict]
    :type ifmt:     str

    :return:        A count of the number of error level events that occurred.
    :rtype:         int

    :raise Exception:   If the JSON template file is malformed.
    """

    # ----------------------------------------
    # Get a loader based on file format.
    if ifmt.upper() == 'YAML':
        loader = yaml.safe_load
    elif ifmt.upper() == 'JSON':
        loader = json.load
    else:
        raise ValueError('Unknown input format: {}'.format(ifmt))

    # ----------------------------------------
    # Load and analyse the CloudFormation template.
    try:
        template_obj = loader(ifp)
    except Exception as e:
        raise Exception('{}: Could not load file - {}'.format(ifp.name, e))

    cfdoc = CFdoc(name if name else ifp.name, template_obj)

    # ----------------------------------------
    # Run the plugins

    if plugins:
        for p in plugins:
            LOG.debug('Running plugin %s', p)
            pp = params.get(p, {}) if params else {}
            try:
                getattr(plugins[p], CFD_PLUGIN)(cfdoc, **pp)
            except Exception as e:
                raise Exception('Plugin {}: {}'.format(p, e))

    # ----------------------------------------
    # Render the output
    LOG.debug('Rendering in format %s', ofmt)
    print(cfdoc.render(fmt=ofmt), file=ofp)


# ------------------------------------------------------------------------------
def find_plugins(module_name, callable_name, skip=()):
    """
    Locate plugins in the specified module that have the named callable.

    :param module_name:     Name of the module containing the plugins.
    :param callable_name:   Name of the callable entry point each plugin must
                            have. Python files that don't have this are ignored.
    :param skip:            Skip any plugin names in this iterable.
    :type module_name:      str
    :type callable_name:    str
    :type skip:             set(str) | list[str] | tuple[str]

    :return:                An OrderedDict of plugin modules, keyed and sorted
                            by plugin name.
    :rtype:                 OrderedDict
    :raise Exception:       If any of the plugins cannot be loaded.
    """

    # Find location of parent module, then scan .py[c] files in that directory
    # for those that have a callable with the specified name.  Files starting
    # with _ or . are ignored.

    LOG.debug('Loading module %s to scan for plugins', module_name)

    parent_module = import_by_name(module_name)

    module_dir = dirname(parent_module.__file__)

    plugins = OrderedDict()
    for fname in sorted(os.listdir(module_dir)):

        # Pick out Python files that don't start with . or _
        m = re.match(r'([^._].*).pyc?$', fname, re.IGNORECASE)
        if not m:
            continue

        plugin_name = m.groups()[0]

        # Ignore any that are already imported or that are to be excluded.
        if plugin_name in plugins or plugin_name in skip:
            continue

        LOG.debug('Checking %s in module %s for plugin-ness', fname, module_name)

        # Import each py file and look for the required entry point callable
        try:
            plugin = import_by_name(plugin_name, module_name)
        except ImportError as e:
            raise Exception('Plugin {}: {}'.format(plugin_name, e))
        entry_point = getattr(plugin, callable_name, None)
        if entry_point and hasattr(entry_point, '__call__'):
            plugins[plugin_name] = plugin
            LOG.debug('Plugin %s loaded', plugin_name)

    return plugins


# ------------------------------------------------------------------------------
def load_plugin(p):
    """
    Load a plugin.

    :param p:       Plugin name.`
    :type p:        str

    :return:        The plugin it will contain a callable attribute with the
                    name cfd_plugin.
    :rtype:         module
    :raise Exception:   If the plugin does not exist or is malformed.
    """

    try:
        plugin = import_by_name(p, 'plugins')
    except ImportError as e:
        raise Exception('Plugin {}: {}'.format(p, e))

    entry_point = getattr(plugin, CFD_PLUGIN, None)
    if not entry_point:
        raise Exception('Plugin {}: Cannot find entry point'.format(p))

    if not hasattr(entry_point, '__call__'):
        raise Exception('Plugin {}: Entry point is not callable'.format(p))

    LOG.debug('Plugin %s loaded', p)

    return plugin


# ------------------------------------------------------------------------------
class ColourLogHandler(logging.Handler):
    """
    Basic stream handler that writes to stderr but with different colours for
    different message levels.

    """

    # --------------------------------------------------------------------------
    def __init__(self, colour=True):
        """
        Allow colour to be enabled or disabled.

        :param colour:      If True colour is enabled for log messages.
                            Default True.
        :type colour:       bool

        """

        super(ColourLogHandler, self).__init__()
        self.colour = colour

    # --------------------------------------------------------------------------
    def emit(self, record):
        """
        Print the record to stderr with some colour enhancement.

        :param record:  Log record
        :type record:   logging.LogRecord
        :return:
        """

        if self.colour:
            if record.levelno >= logging.ERROR:
                colour = Style.BRIGHT + Fore.RED
            elif record.levelno >= logging.WARNING:
                colour = Fore.MAGENTA
            elif record.levelno >= logging.INFO:
                colour = Fore.BLACK
            else:
                colour = Style.DIM + Fore.BLACK

            print(colour + self.format(record) + Fore.RESET + Style.RESET_ALL, file=sys.stderr)
        else:
            print(self.format(record), file=sys.stderr)


# ------------------------------------------------------------------------------
# noinspection PyUnresolvedReferences
class StoreNameValuePair(argparse.Action):
    """
    Used with argparse to store values from options of the form:
        --option name=value

    The destination (self.dest) will be created as a dict {name: value}. This
    allows multiple name-value pairs to be set for the same option.

    Usage is:

        argparser.add_argument('-x', metavar='key=value', action=StoreNameValuePair)

    """

    # --------------------------------------------------------------------------
    def __call__(self, parser, namespace, values, option_string=None):
        try:
            n, v = values.split('=')
        except ValueError:
            raise argparse.ArgumentError(self, 'Argument must be key=value')
        if not hasattr(namespace, self.dest) or not getattr(namespace, self.dest):
            setattr(namespace, self.dest, dict())
        getattr(namespace, self.dest)[n] = v


# ------------------------------------------------------------------------------
def main():
    """
    Main

    :return:    status
    :rtype:     int
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Extract documentation from an AWS CloudFormation template.',
        epilog='For more information: {}'.format(CFD_URL)
    )

    argp.add_argument('-c', '--no-colour', '--no-color', dest='no_colour', action='store_true',
                      default=False, help='Don\'t use colour in information messages.')
    argp.add_argument('-d', '--define', metavar='plugin.param=value',
                      dest='plugin_params', action=StoreNameValuePair,
                      help='Define params for plugins.')
    argp.add_argument('-f', '--format', action='store', default='html',
                      help='Output format (default html). Must correspond to a'
                           ' Jinja2 template named FORMAT.jinja2')
    argp.add_argument('-l', '--level', metavar='LEVEL', default='warning',
                      help='Print messages of a given severity level or above.'
                           ' The standard logging level names are available but info,'
                           ' warning and error are most useful. The Default is warning.')
    argp.add_argument('--list-plugins', dest='list_plugins', action='store_true',
                      help='List available plugins and exit.')
    argp.add_argument('-n', '--name',
                      help='CloudFormation template name (not file name).'
                           ' If not specified the name of the input file is used.')
    argp.add_argument('-p', '--plugin', metavar='plugin1,plugin2...', action='append',
                      help='Enable only the specified plugin(s).'
                           ' Can be used more than once.'
                           ' By default, all available plugins are enabled.')
    argp.add_argument('-P', '--disable-plugin', dest='notplugins', action='append',
                      metavar='plugin1,plugin2...',
                      help='Disable the specified plugin(s).'
                           ' Can be used more than once.'
                           ' By default, all available plugins are enabled.')
    argp.add_argument('-y', '--yaml', action='store_true',
                      help='The CloudFormation template is in YAML format. This is'
                           ' required when the format cannot be guessed from the'
                           ' file suffix. JSON format is assumed by default.')
    argp.add_argument('template', nargs='?', type=argparse.FileType('r'), default=sys.stdin,
                      help='CloudFormation template. Default stdin.')

    args = argp.parse_args()

    # ----------------------------------------
    # Setup some logging

    LOG.addHandler(ColourLogHandler(colour=not args.no_colour))
    LOG.setLevel(get_log_level(args.level))
    LOG.debug('log level set to %s', LOG.getEffectiveLevel())

    # ----------------------------------------
    # Load plugins. Each plugin must have a callable element with the name defined
    # in CFD_PLUGIN. It should also have a 'description' string.

    # Get the set of plugins that should not be loaded.
    notplugins = set()
    if args.notplugins:
        for plugin in args.notplugins:
            notplugins.update(set(plugin.split(',')))

    if args.plugin:
        plugins = OrderedDict()
        for plugin in args.plugin:
            for p in plugin.split(','):
                if p not in notplugins:
                    plugins[p] = load_plugin(p)
    else:
        # Load all available plugins
        plugins = find_plugins('plugins', CFD_PLUGIN, skip=notplugins)

    if args.list_plugins:
        # Just print descriptive information. This is extracted from the first
        # line of the docstring in the plugin.
        for p in plugins:
            print('{}: {}'.format(
                p,
                getattr(plugins[p], '__doc__', 'No info available').strip().splitlines()[0])
            )
        return 0

    # ----------------------------------------
    # Gather any plugin args. Each one must be of the form plugin.param=value.

    plugin_params = {pname: {} for pname in plugins}
    if args.plugin_params:
        for plug_param in args.plugin_params:
            try:
                plugin_name, param_name = plug_param.split('.', 1)
            except ValueError:
                raise Exception('Invalid plugin parameter name: {}'.format(plug_param))

            try:
                plugin_params[plugin_name][param_name] = args.plugin_params[plug_param]
            except KeyError:
                raise Exception(
                    'Plugin {} is not loaded - cannot set parameters for it'.format(plugin_name))

    # ----------------------------------------
    # Do the business.

    ext = os.path.splitext(args.template.name)[1].lower()
    dofile(ofmt=args.format, ifp=args.template, ofp=sys.stdout,
           name=args.name, plugins=plugins, params=plugin_params,
           ifmt='YAML' if args.yaml or ext in ('.yml', '.yaml') else 'JSON')


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    try:
        exit(main())
    except KeyboardInterrupt:
        print('Interrupt', file=sys.stderr)
        exit(1)
    except Exception as ex:
        print('{}: {}'.format(PROG, ex), file=sys.stderr)
        exit(1)
